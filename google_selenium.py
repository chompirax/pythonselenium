import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome()
browser.get('http://www.google.com')

search = browser.find_element_by_name('q')
search.send_keys("pruebaz")
search.send_keys(Keys.RETURN)
time.sleep(2) 
browser.find_element_by_link_text("pruebas").click()
time.sleep(2)
list = browser.find_elements_by_xpath("//div[@class='srg']/div[@class='g']")
if(len(list)>6):
	print("La cantidad de resultados es mayor a 6")

time.sleep(5)
browser.quit()
